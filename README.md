# KEXP-RadioApp
An internet radio player for KEXP for modern Android OSs.

This Android app features:
1. A live stream of KEXP that a user can start/ stop and can be played in the background.
2. A live update that shows information about the song, artists, and album.
3. A live update on information about the current DJ.
4. A history activity that displays the past songs that were played from the stream.
5. A donation button! To keep KEXP on the air without adds!
